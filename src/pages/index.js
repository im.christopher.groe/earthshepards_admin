import { Box, Button, ImageList, ImageListItem, Input, Table, TextField, Typography } from '@mui/material';
import react, { useEffect, useState } from 'react';
import CustomTable from '../components/table/CustomTable';
import { getUsers, saveNotification } from '../helper';
import EnhancedTable from '../components/table/CheckboxTable';

const Page = () => {
    const [users, setUsers] = useState([]);
    const [notification, setNotification] = useState('');
    const [week, setWeek] = useState(0);

    useEffect(() => {
        (async () => {
            const data = await getUsers();
            setUsers(data);
        })()
    }, []);

    const handleNotificationClick = async () => {
        await saveNotification({ text: notification, week });
    };

    return (
        <Box sx={{ display: 'flex', justifyContent: 'center', textAlign: 'center', width: '100%' }}>
            <Box sx={{ width: '80%', pt: 10 }}>
                {/* <Typography>Users</Typography> */}
                {/* <CustomTable data={users} setUsers={setUsers} /> */}
                <Box sx={{ }}>
                    Notification: <TextField sx={{ mr: 10, width: 600}} value={notification} onChange={(e) => setNotification(e.target.value)} multiline></TextField>
                    Week: <Input sx={{ width: 50, mr: 10}} value={week} onChange={(e) => setWeek(e.target.value)} />
                    <Button onClick={handleNotificationClick} variant="contained">Save</Button>
                </Box>

                <EnhancedTable data={users} setUsers={setUsers} />
            </Box>
        </Box> 
    );
}

export default Page;